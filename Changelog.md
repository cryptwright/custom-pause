# Custom Pause Foundry VTT Module Changelog

## 1.0.0 Initial release

- Ability to set custom pause icon for your game.

## 1.0.1 Bug Fix

- Fixed typo in module.json

## 1.1.0 Enhancement

- Added phrase customisation (Septermber, 2022)