Hooks.on("init", () => {

  // Image Selection
  game.settings.register("custom-pause", "chooseFile", {
    name: "Select Image",
    hint: "Select an image for your pause icon. Reload when finished.",
    scope: "world",
    config: true,
    type: String,
    default: "",
    filePicker: true // This is the important part
    });
  
  // Phrase Selection
  game.settings.register("custom-pause", "phrase", {
      name: "Select Phrase",
      hint: "Select a phrase to say above your animation. Reload when finished.",
      scope: "world",
      config: true,
      type: String,
      default: "Game Paused"
      });  
  
  });
  
  Hooks.on("renderPause", (app, html, options) => {
  
      if (options.paused) {
  
        // Image Injection
        html.find("img")[0].src = (game.settings.get("custom-pause", "chooseFile"));
  
        // Phrase Injection
        html.find("figcaption")[0].innerHTML = (game.settings.get("custom-pause", "phrase"));
      }
    });
  